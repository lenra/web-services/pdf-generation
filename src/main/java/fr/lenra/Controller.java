package fr.lenra;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.io.source.ByteArrayOutputStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class Controller {
	@Value("${service.api-key}")
	public String serviceApiKey;

	@RequestMapping(path = "/html2pdf", method = RequestMethod.POST, produces = MediaType.APPLICATION_PDF_VALUE)
	public byte[] html2pdf(@RequestHeader(name = "Auth", required = true) String apiKey,
			@RequestParam(value = "html", required = true) MultipartFile html)
			throws IOException, IllegalAccessException {
		if (!serviceApiKey.equals(apiKey))
			throw new IllegalAccessException();

		Path folder = Files.createTempDirectory("html2pdf_");

		java.io.InputStream in = null;

		List<File> files = new ArrayList<>();

		if (html.getContentType().equals(MediaType.TEXT_HTML_VALUE))
			in = html.getInputStream();
		else if (html.getOriginalFilename().toLowerCase().endsWith(".zip")) {
			ZipInputStream zIn = new ZipInputStream(html.getInputStream());
			ZipEntry ze;
			while ((ze = zIn.getNextEntry()) != null) {
				String name = ze.getName();
				System.out.println(name);
				if (ze.isDirectory())
					continue;
				if (name.equals("index.html")) {
					try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
						StreamUtils.copy(zIn, bos);
						in = new ByteArrayInputStream(bos.toByteArray());
					}
				} else {
					File f = folder.resolve(name).toFile();
					f.getParentFile().mkdirs();
					try (FileOutputStream fos = new FileOutputStream(f);
							BufferedOutputStream bos = new BufferedOutputStream(fos)) {
						StreamUtils.copy(zIn, bos);
					}
					files.add(f);
				}
				zIn.closeEntry();
			}
			zIn.close();
		} else
			throw new IllegalArgumentException();

		ConverterProperties properties = new ConverterProperties();
		properties.setBaseUri(folder.toString());

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		HtmlConverter.convertToPdf(in, out, properties);

		files.stream().forEach(f -> {
			try {
				Files.delete(f.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		});

		folder.toFile().deleteOnExit();

		return out.toByteArray();
	}

	@RequestMapping(path = "/test.html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
	public byte[] getTest() throws IOException {
		byte[] buffer = new byte[2048];
		InputStream in = Controller.class.getResourceAsStream("/test.html");
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		int len;
		while ((len = in.read(buffer)) > 0) {
			out.write(buffer, 0, len);
		}
		out.close();
		return out.toByteArray();
	}
}